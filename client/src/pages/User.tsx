import * as React from "react";
import { RouteComponentProps } from "react-router";
import { connect } from "react-redux";

import { IUserProfile } from "../redux/types/userTypes";
import { IInitialState } from "../redux/store";
import { Grid } from "@material-ui/core";
import Scream from "../components/Scream";
import StaticProfile from "../components/StaticProfile";
import { getUserProfile } from "../redux/actions/dataActions";

interface IUserState {
  profile?: IUserProfile;
  screamIdParam: string | null;
}

interface IRouterProps {
  handle: string;
  screamId: string;
}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;

type UserProps = StateProps & DispatchProps & RouteComponentProps<IRouterProps>;

class User extends React.Component<UserProps, IUserState> {
  state: IUserState = {
    screamIdParam: null
  };

  async componentDidMount() {
    const {
      match: {
        params: { handle, screamId }
      },
      getUserProfile
    } = this.props;
    if (screamId) {
      this.setState({ screamIdParam: screamId });
    }

    await getUserProfile(handle);
  }

  renderScreamsList = () => {
    const { staticUserProfile } = this.props;
    if (!staticUserProfile) return;
    const { screams } = staticUserProfile;
    const { screamIdParam } = this.state;

    return screams.map(scream => (
      <Scream
        key={scream.screamId}
        scream={scream}
        openDialog={scream.screamId === screamIdParam}
      />
    ));
  };

  renderLoading = () => {
    const { staticUserProfile } = this.props;
    if (!staticUserProfile) {
      return <p>Loading...</p>;
    }

    if (staticUserProfile && !staticUserProfile.screams.length) {
      return <p>No Screams for this User.</p>;
    }
  };

  renderProfile = () => {
    const { staticUserProfile } = this.props;
    if (!staticUserProfile) {
      return this.renderLoading();
    }
    return <StaticProfile userProfile={staticUserProfile.user} />;
  };

  render() {
    return (
      <Grid container spacing={16}>
        <Grid item sm={8} xs={12}>
          {this.renderLoading()}
          {this.renderScreamsList()}
        </Grid>
        <Grid item sm={4} xs={12}>
          {this.renderProfile()}
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = ({ data: { staticUserProfile } }: IInitialState) => ({
  staticUserProfile
});

const mapDispatchToProps = (dispatch: any) => ({
  getUserProfile: (handle: string) => dispatch(getUserProfile(handle))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(User);
