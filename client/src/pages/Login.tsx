import * as React from "react";
import { TextAlignProperty } from "csstype";
import { RouterProps } from "react-router";
import { connect } from "react-redux";

// Material UI
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";

import AppIcon from "../images/icon.png";

import { loginUser } from "../redux/actions/userActions";
import { IInitialState } from "../redux/store";
import { ILoginActionParams } from "../redux/types/userTypes";

const styles = {
  form: {
    textAlign: "center" as TextAlignProperty
  },
  image: {
    margin: "20px auto 20px auto"
  },
  pageTitle: {
    margin: "10px auto 10px auto"
  },
  textField: {
    margin: "10px auto 10px auto"
  },
  button: {
    marignTop: 20
  }
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StyleProps = WithStyles<typeof styles>;

type LoginProps = StateProps & DispatchProps & StyleProps & RouterProps;

interface ILoginState {
  email: string;
  password: string;
  loading: boolean;
  errors: any;
}

class Login extends React.Component<LoginProps, ILoginState> {
  state: ILoginState = {
    email: "",
    password: "",
    loading: false,
    errors: {}
  };

  handleSubmit = async (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser({
      loginData: userData,
      history: this.props.history
    });
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [event.target.name]: event.target.value
    } as Pick<ILoginState, any>);
  };

  render() {
    const {
      classes,
      UI: { loading }
    } = this.props;
    const { email, password, errors } = this.state;
    return (
      <Grid container className={classes.form}>
        <Grid item sm />
        <Grid item sm>
          <img src={AppIcon} alt="Monkey Image" className={classes.image} />
          <Typography variant="h2" className={classes.pageTitle}>
            Login
          </Typography>
          <form noValidate onSubmit={this.handleSubmit}>
            <TextField
              id="email"
              name="email"
              type="email"
              label="Email"
              className={classes.textField}
              value={email}
              onChange={this.handleChange}
              fullWidth
              helperText={errors.email}
              error={errors.email && true}
            />
            <TextField
              id="password"
              name="password"
              type="password"
              label="Password"
              className={classes.textField}
              value={password}
              onChange={this.handleChange}
              fullWidth
              helperText={errors.password}
              error={errors.password && true}
            />
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.button}
              disabled={loading}
            >
              Login
              {loading && (
                <CircularProgress size={30} style={{ position: "absolute" }} />
              )}
            </Button>
          </form>
        </Grid>
        <Grid item sm />
      </Grid>
    );
  }
}

const mapStateToProps = (store: IInitialState) => ({
  user: store.user,
  UI: store.UI
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    loginUser: (param: ILoginActionParams) => dispatch(loginUser(param))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Login));
