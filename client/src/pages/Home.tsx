import * as React from "react";
import { connect } from "react-redux";

// Material UI
import Grid from "@material-ui/core/Grid";

import Scream from "../components/Scream";
import Profile from "../components/Profile";
import { IInitialState } from "../redux/store";
import { getScreams } from "../redux/actions/dataActions";
import ScreamSkeleton from "../components/ScreamSkeleton";

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type HomeProps = StateProps & DispatchProps;

class Home extends React.Component<HomeProps> {
  async componentDidMount() {
    this.props.getScreams();
  }

  render() {
    const {
      data: { screams, loading }
    } = this.props;
    let recentScreamsMarkUp = !loading ? (
      screams.map(scream => <Scream key={scream.screamId} scream={scream} />)
    ) : (
      <ScreamSkeleton />
    );

    return (
      <Grid container spacing={16}>
        <Grid item sm={8} xs={12}>
          {recentScreamsMarkUp}
        </Grid>
        <Grid item sm={4} xs={12}>
          <Profile />
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = (state: IInitialState) => ({
  data: state.data
});

const mapDispatchToProps = (dispatch: any) => ({
  getScreams: () => dispatch(getScreams())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
