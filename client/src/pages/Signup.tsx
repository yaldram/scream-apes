import * as React from "react";
import axios from "axios";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { TextAlignProperty } from "csstype";
import { RouterProps } from "react-router";
import { connect } from "react-redux";

// Material UI
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";

import AppIcon from "../images/icon.png";
import { IInitialState } from "../redux/store";
import { signUpUser } from "../redux/actions/userActions";
import { ISignUpActionParams, IUserState } from "../redux/types/userTypes";
import { IUiState } from "../redux/types/uiTypes";

const styles = {
  form: {
    textAlign: "center" as TextAlignProperty
  },
  image: {
    margin: "20px auto 20px auto"
  },
  pageTitle: {
    margin: "10px auto 10px auto"
  },
  textField: {
    margin: "10px auto 10px auto"
  },
  button: {
    marignTop: 20
  }
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StyleProps = WithStyles<typeof styles>;

type SignUpProps = StateProps & DispatchProps & StyleProps & RouterProps;

interface ISignUpState {
  email: string;
  password: string;
  confirmPassword: string;
  handle: string;
  errors: any;
}

class SignUp extends React.Component<SignUpProps, ISignUpState> {
  state: ISignUpState = {
    email: "",
    password: "",
    confirmPassword: "",
    handle: "",
    errors: {}
  };

  componentWillReceiveProps({ UI: { errors } }: SignUpProps) {
    if (errors) {
      this.setState({ errors });
    }
  }

  handleSubmit = async (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
    const { email, password, confirmPassword, handle } = this.state;
    this.props.signupUser({
      signUpData: { email, password, confirmPassword, handle },
      history: this.props.history
    });
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [event.target.name]: event.target.value
    } as Pick<ISignUpState, any>);
  };

  render() {
    const {
      classes,
      UI: { loading }
    } = this.props;
    const { email, password, errors, confirmPassword, handle } = this.state;

    return (
      <Grid container className={classes.form}>
        <Grid item sm />
        <Grid item sm>
          <img src={AppIcon} alt="Monkey Image" className={classes.image} />
          <Typography variant="h2" className={classes.pageTitle}>
            SignUp
          </Typography>
          <form noValidate onSubmit={this.handleSubmit}>
            <TextField
              id="email"
              name="email"
              type="email"
              label="Email"
              className={classes.textField}
              value={email}
              onChange={this.handleChange}
              fullWidth
              helperText={errors.email}
              error={errors.email && true}
            />
            <TextField
              id="password"
              name="password"
              type="password"
              label="Password"
              className={classes.textField}
              value={password}
              onChange={this.handleChange}
              fullWidth
              helperText={errors.password}
              error={errors.password && true}
            />
            <TextField
              id="confirmPassword"
              name="confirmPassword"
              type="password"
              label="confirmPassword"
              className={classes.textField}
              value={confirmPassword}
              onChange={this.handleChange}
              fullWidth
              helperText={errors.password}
              error={errors.confirmPassword && true}
            />
            <TextField
              id="handle"
              name="handle"
              type="text"
              label="handle"
              className={classes.textField}
              value={handle}
              onChange={this.handleChange}
              fullWidth
              helperText={errors.handle}
              error={errors.handle && true}
            />
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.button}
              disabled={loading}
            >
              SignUp
              {loading && (
                <CircularProgress size={30} style={{ position: "absolute" }} />
              )}
            </Button>
          </form>
        </Grid>
        <Grid item sm />
      </Grid>
    );
  }
}

const mapStateToProps = (state: IInitialState) => ({
  user: state.user,
  UI: state.UI
});

const mapDispatchToProps = (dispatch: any) => ({
  signupUser: (param: ISignUpActionParams) => dispatch(signUpUser(param))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(SignUp));
