import { Reducer } from "redux";
import { IUiState, UiActions, UiReducerActionTypes } from "../types/uiTypes";

const initialState: IUiState = {
  loading: false,
  errors: null
};

export const uiReducer: Reducer<IUiState, UiActions> = (
  state = initialState,
  actions
) => {
  switch (actions.type) {
    case UiReducerActionTypes.SET_ERRORS: {
      return {
        ...state,
        loading: false,
        errors: actions.errors
      };
    }

    case UiReducerActionTypes.CLEAR_ERRORS: {
      return {
        ...state,
        loading: false,
        errors: null
      };
    }

    case UiReducerActionTypes.LOADING_UI: {
      return {
        ...state,
        loading: true
      };
    }

    case UiReducerActionTypes.STOP_LOADING_UI: {
      return {
        ...state,
        loading: false
      };
    }
  }

  return state;
};
