import { Reducer } from "redux";
import {
  IDataState,
  DataActions,
  DataReducerActionTypes
} from "../types/dataTypes";

const initialState: IDataState = {
  screams: [],
  loading: false
};

export const dataReducer: Reducer<IDataState, DataActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case DataReducerActionTypes.LOADING_DATA: {
      return {
        ...state,
        loading: true
      };
    }

    case DataReducerActionTypes.SET_SCREAMS: {
      return {
        ...state,
        screams: action.screams,
        loading: false
      };
    }

    case DataReducerActionTypes.GET_SCREAM: {
      return {
        ...state,
        scream: action.scream
      };
    }

    case DataReducerActionTypes.LIKE_SCREAM:
    case DataReducerActionTypes.UNLIKE_SCREAM: {
      let index = state.screams.findIndex(
        scream => scream.screamId === action.scream.screamId
      );
      state.screams[index] = action.scream;
      return { ...state };
    }

    case DataReducerActionTypes.POST_SCREAM: {
      return {
        ...state,
        screams: [action.scream, ...state.screams]
      };
    }

    case DataReducerActionTypes.DELETE_SCREAM: {
      let { screams: currentScreams } = state;
      currentScreams = currentScreams.filter(
        scream => scream.screamId !== action.screamId
      );
      return {
        ...state,
        screams: currentScreams
      };
    }
    case DataReducerActionTypes.POST_COMMENT: {
      if (!state.scream) {
        return state;
      }
      return {
        ...state,
        scream: {
          ...state.scream,
          comments: [action.comment, ...state.scream.comments]
        }
      };
    }

    case DataReducerActionTypes.CLEAR_SCREAM: {
      return {
        ...state,
        scream: undefined
      };
    }

    case DataReducerActionTypes.GET_USER_PROFILE: {
      return {
        ...state,
        loading: false,
        staticUserProfile: action.staticUserProfile
      };
    }
  }

  return state;
};
