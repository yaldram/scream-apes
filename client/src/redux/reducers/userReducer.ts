import { Reducer } from "redux";
import { IUserState, UserActions, UserActionTypes } from "../types/userTypes";

const initialState: IUserState = {
  authenticated: false,
  loading: false,
  likes: [],
  notifications: []
};

export const userReducer: Reducer<IUserState, UserActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case UserActionTypes.SET_AUTHENTICATED: {
      return {
        ...state,
        authenticated: true
      };
    }
    case UserActionTypes.SET_UNAUTHENTICATED: {
      return initialState;
    }

    case UserActionTypes.SET_USER: {
      const {
        userInfo: { userDetails: userCredentials, likes, notifications }
      } = action;
      return {
        ...state,
        authenticated: true,
        loading: false,
        userCredentials,
        likes,
        notifications
      };
    }

    case UserActionTypes.LOADING_USER: {
      return {
        ...state,
        loading: true
      };
    }

    case UserActionTypes.LIKE_SCREAM: {
      if (state.userCredentials) {
        return {
          ...state,
          likes: [
            ...state.likes,
            {
              userHandle: state.userCredentials.handle,
              screamId: action.scream.screamId
            }
          ]
        };
      }
    }

    case UserActionTypes.UNLIKE_SCREAM: {
      return {
        ...state,
        likes: state.likes.filter(
          like => like.screamId !== action.scream.screamId
        )
      };
    }

    case UserActionTypes.MARK_NOTIFICATIONS_READ: {
      const notifications = state.notifications.map(notification => ({
        ...notification,
        read: true
      }));
      return {
        ...state,
        notifications
      };
    }
  }

  return state;
};
