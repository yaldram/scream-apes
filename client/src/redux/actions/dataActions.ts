import axios from "axios";
import { ActionCreator, AnyAction, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";
import {
  IGetAllScreamsAction,
  DataReducerActionTypes,
  ILikeScreamAction,
  IScream,
  IUnlikeScreamAction,
  IDeleteScreamAction,
  IPostScreamAction,
  IGetScreamAction,
  IDataLoadingAction,
  IComment,
  ICommentAction,
  IStaticUserProfile,
  IGetUserProfileAction
} from "../types/dataTypes";
import {
  UiReducerActionTypes,
  IUiClearErorrsAction,
  IUiSetErrorsAction,
  IUiLoadingAction,
  IUiStopLoadingAction
} from "../types/uiTypes";
import { IUser } from "../types/userTypes";

export const getScreams: ActionCreator<
  ThunkAction<Promise<AnyAction>, null, IScream[], IGetAllScreamsAction>
> = () => {
  return async (dispatch: Dispatch) => {
    dispatch<IDataLoadingAction>({ type: DataReducerActionTypes.LOADING_DATA });
    try {
      const { data } = await axios.get("/screams");
      return dispatch({
        type: DataReducerActionTypes.SET_SCREAMS,
        screams: data
      });
    } catch (error) {
      return dispatch({
        type: DataReducerActionTypes.SET_SCREAMS,
        screams: []
      });
    }
  };
};

export const getScream: ActionCreator<
  ThunkAction<Promise<void>, { screamId: string }, IScream, IGetScreamAction>
> = (screamId: string) => {
  return async (dispatch: Dispatch) => {
    try {
      dispatch<IUiLoadingAction>({ type: UiReducerActionTypes.LOADING_UI });
      const { data: scream } = await axios.get<IScream>(`/screams/${screamId}`);
      dispatch<IGetScreamAction>({
        type: DataReducerActionTypes.GET_SCREAM,
        scream
      });
      dispatch<IUiStopLoadingAction>({
        type: UiReducerActionTypes.STOP_LOADING_UI
      });
    } catch (error) {
      console.log("Error getScream Action", error);
    }
  };
};

export const likeScream: ActionCreator<
  ThunkAction<Promise<void>, { screamId: string }, IScream, ILikeScreamAction>
> = (screamId: string) => {
  return async (dispatch: Dispatch) => {
    const { data: scream } = await axios.get<IScream>(
      `/screams/${screamId}/like`
    );
    dispatch<ILikeScreamAction>({
      type: DataReducerActionTypes.LIKE_SCREAM,
      scream
    });
  };
};

export const unlikeScream: ActionCreator<
  ThunkAction<Promise<void>, { screamId: string }, IScream, IUnlikeScreamAction>
> = (screamId: string) => {
  return async (dispatch: Dispatch) => {
    const { data: scream } = await axios.get<IScream>(
      `/screams/${screamId}/unlike`
    );
    dispatch<IUnlikeScreamAction>({
      type: DataReducerActionTypes.UNLIKE_SCREAM,
      scream
    });
  };
};

export const deleteScream: ActionCreator<
  ThunkAction<
    Promise<void>,
    { screamId: string },
    { screamId: string },
    IDeleteScreamAction
  >
> = (screamId: string) => {
  return async (dispatch: Dispatch) => {
    await axios.delete(`/screams/${screamId}`);
    dispatch<IDeleteScreamAction>({
      type: DataReducerActionTypes.DELETE_SCREAM,
      screamId
    });
  };
};

export const postScream: ActionCreator<
  ThunkAction<
    Promise<void>,
    { screamBody: string },
    { scream: IScream },
    IPostScreamAction
  >
> = (screamBody: string) => {
  return async (dispatch: Dispatch) => {
    try {
      const {
        data: { scream }
      } = await axios.post<{ scream: IScream }>("/screams", {
        body: screamBody
      });
      dispatch<IUiLoadingAction>({ type: UiReducerActionTypes.LOADING_UI });
      dispatch<IPostScreamAction>({
        type: DataReducerActionTypes.POST_SCREAM,
        scream
      });
      dispatch<any>(clearErrors());
    } catch (errors) {
      dispatch<IUiSetErrorsAction>({
        type: UiReducerActionTypes.SET_ERRORS,
        errors
      });
    }
  };
};
export const postComment: ActionCreator<
  ThunkAction<
    Promise<void>,
    { screamId: string; commentBody: { comment: string } },
    IComment,
    ICommentAction
  >
> = (screamId: string, commentBody: { comment: string }) => {
  return async (dispatch: Dispatch) => {
    try {
      dispatch<IUiLoadingAction>({ type: UiReducerActionTypes.LOADING_UI });
      const { data: comment } = await axios.post(
        `/screams/${screamId}/comment`,
        commentBody
      );
      dispatch<ICommentAction>({
        type: DataReducerActionTypes.POST_COMMENT,
        comment
      });
      dispatch<any>(clearErrors());
    } catch (errors) {
      dispatch<IUiSetErrorsAction>({
        type: UiReducerActionTypes.SET_ERRORS,
        errors
      });
    }
  };
};

export const getUserProfile: ActionCreator<
  ThunkAction<
    Promise<void>,
    IStaticUserProfile,
    { handle: string },
    IGetUserProfileAction
  >
> = (handle: string) => async (dispatch: Dispatch) => {
  dispatch<IDataLoadingAction>({ type: DataReducerActionTypes.LOADING_DATA });
  try {
    const { data: staticUserProfile } = await axios.get<IStaticUserProfile>(
      `/user/${handle}`
    );
    dispatch<IGetUserProfileAction>({
      type: DataReducerActionTypes.GET_USER_PROFILE,
      staticUserProfile
    });
  } catch (error) {
    dispatch<IUiSetErrorsAction>({
      type: UiReducerActionTypes.SET_ERRORS,
      errors: "Error Fetching User Profile"
    });
  }
};

export const clearLoadedScream = () => ({
  type: DataReducerActionTypes.CLEAR_SCREAM
});

export const clearErrors = () => (dispatch: Dispatch) => {
  dispatch<IUiClearErorrsAction>({ type: UiReducerActionTypes.CLEAR_ERRORS });
};
