import { ActionCreator, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";
import axios from "axios";
import {
  IUserDetials,
  ILoginActionParams,
  ISignUpActionParams,
  IGetUserDetialsAction,
  UserActionTypes,
  IUser,
  ILoadingUserAction,
  IUserProfile,
  ILikeUnlikeScream,
  IMarkNotificationsAction,
  INotifications
} from "../types/userTypes";
import {
  UiReducerActionTypes,
  IUiSetErrorsAction,
  IUiClearErorrsAction,
  IUiStopLoadingAction
} from "../types/uiTypes";

export const loginUser: ActionCreator<
  ThunkAction<
    Promise<void>,
    IUserDetials,
    ILoginActionParams,
    IGetUserDetialsAction
  >
> = ({ loginData, history }: ILoginActionParams) => async (
  dispatch: Dispatch
) => {
  try {
    dispatch({
      type: UiReducerActionTypes.LOADING_UI
    });
    const { data } = await axios.post("/login", loginData);
    setAuthorizationHeader(data.token);
    dispatch({ type: UiReducerActionTypes.CLEAR_ERRORS });
    dispatch<any>(getUserData());
    history.push("/");
  } catch (error) {
    dispatch({
      type: UiReducerActionTypes.SET_ERRORS,
      errors: error.response.data
    });
  }
};

export const getUserData: ActionCreator<
  ThunkAction<Promise<void>, IUserDetials, null, IGetUserDetialsAction>
> = () => async (dispatch: Dispatch) => {
  dispatch<ILoadingUserAction>({ type: UserActionTypes.LOADING_USER });
  const {
    data: { credentials, likes, notifications }
  } = await axios.get<{
    credentials: IUser;
    likes: ILikeUnlikeScream[];
    notifications: INotifications[];
  }>("/user");
  dispatch<IGetUserDetialsAction>({
    type: UserActionTypes.SET_USER,
    userInfo: { userDetails: credentials, likes, notifications }
  });
};

export const signUpUser: ActionCreator<
  ThunkAction<
    Promise<void>,
    IUserDetials,
    ISignUpActionParams,
    IGetUserDetialsAction
  >
> = (userData: ISignUpActionParams) => async (dispatch: Dispatch) => {
  dispatch({ type: UiReducerActionTypes.LOADING_UI });
  try {
    const { data } = await axios.post("/signup", userData.signUpData);
    setAuthorizationHeader(data.token);
    dispatch<any>(getUserData());
    dispatch<IUiClearErorrsAction>({ type: UiReducerActionTypes.CLEAR_ERRORS });
    userData.history.push("/");
  } catch (error) {
    dispatch<IUiSetErrorsAction>({
      type: UiReducerActionTypes.SET_ERRORS,
      errors: error.response.data
    });
  }
};

export const editProfile: ActionCreator<
  ThunkAction<Promise<void>, IUserDetials, IUserProfile, IGetUserDetialsAction>
> = (userData: IUserProfile) => async (dispatch: Dispatch) => {
  dispatch({ type: UiReducerActionTypes.LOADING_UI });
  try {
    await axios.post("/user", userData);
    dispatch<any>(getUserData());
    dispatch<IUiStopLoadingAction>({
      type: UiReducerActionTypes.STOP_LOADING_UI
    });
  } catch (error) {
    dispatch<IUiSetErrorsAction>({
      type: UiReducerActionTypes.SET_ERRORS,
      errors: error.response.data
    });
  }
};

export const logoutUser = () => (dispatch: Dispatch) => {
  localStorage.removeItem("FBIdToken");
  delete axios.defaults.headers.common["Authorization"];
  dispatch({ type: UserActionTypes.SET_UNAUTHENTICATED });
};

export const uploadImage: ActionCreator<
  ThunkAction<Promise<void>, IUserDetials, FormData, IGetUserDetialsAction>
> = (formData: FormData) => async (dispatch: Dispatch) => {
  dispatch<ILoadingUserAction>({ type: UserActionTypes.LOADING_USER });
  try {
    await axios.post("/user/uploadImage", formData);
    dispatch<any>(getUserData());
  } catch (error) {
    dispatch<IUiSetErrorsAction>({
      type: UiReducerActionTypes.SET_ERRORS,
      errors: "Error Uploading Picture"
    });
  }
};

export const markNotifications: ActionCreator<
  ThunkAction<
    Promise<void>,
    IUserDetials,
    { notificationIds: string[] },
    IMarkNotificationsAction
  >
> = (notificationIds: string[]) => async (dispatch: Dispatch) => {
  try {
    await axios.post("/user/notifications", notificationIds);
    dispatch<IMarkNotificationsAction>({
      type: UserActionTypes.MARK_NOTIFICATIONS_READ
    });
  } catch (error) {
    console.log("ERROR NOTIFICATIONS MARK", error);
  }
};

const setAuthorizationHeader = (token: string) => {
  const FBIdToken = `Bearer ${token}`;
  localStorage.setItem("FBIdToken", FBIdToken);
  axios.defaults.headers.common["Authorization"] = FBIdToken;
};
