import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { userReducer } from "./reducers/userReducer";
import { dataReducer } from "./reducers/dataReducer";
import { uiReducer } from "./reducers/uiReducer";
import { IUserState } from "./types/userTypes";
import { IDataState } from "./types/dataTypes";
import { IUiState } from "./types/uiTypes";

export interface IInitialState {
  user: IUserState;
  data: IDataState;
  UI: IUiState;
}

const reducers = combineReducers<IInitialState>({
  user: userReducer,
  data: dataReducer,
  UI: uiReducer
});

const store = createStore(reducers, undefined, compose(applyMiddleware(thunk)));

export default store;
