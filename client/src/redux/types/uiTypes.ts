export enum UiReducerActionTypes {
  SET_ERRORS = "SET_ERRORS",
  CLEAR_ERRORS = "CLEAR_ERRORS",
  LOADING_UI = "LOADING_UI",
  STOP_LOADING_UI = "STOP_LOADING_UI"
}

export interface IUiState {
  loading: boolean;
  errors: string | null;
}

export interface IUiSetErrorsAction {
  type: UiReducerActionTypes.SET_ERRORS;
  errors: string;
}

export interface IUiClearErorrsAction {
  type: UiReducerActionTypes.CLEAR_ERRORS;
}

export interface IUiLoadingAction {
  type: UiReducerActionTypes.LOADING_UI;
}

export interface IUiStopLoadingAction {
  type: UiReducerActionTypes.STOP_LOADING_UI;
}

export type UiActions =
  | IUiSetErrorsAction
  | IUiClearErorrsAction
  | IUiLoadingAction
  | IUiStopLoadingAction;
