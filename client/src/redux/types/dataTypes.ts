import { IUser } from "./userTypes";

export enum DataReducerActionTypes {
  SET_SCREAMS = "SET_SCREAMS",
  LOADING_DATA = "LOADING",
  LIKE_SCREAM = "LIKE_SCREAM",
  UNLIKE_SCREAM = "UNLIKE_SCREAM",
  DELETE_SCREAM = "DELETE_SCREAM",
  POST_SCREAM = "POST_SCREAM",
  GET_SCREAM = "GET_SCREAM",
  CLEAR_SCREAM = "CLEAR_SCREAM",
  POST_COMMENT = "POST_COMMENT",
  GET_USER_PROFILE = "GET_USER_PROFILE"
}

export interface IComment {
  body: string;
  createdAt: string;
  userImage: string;
  userHandle: string;
}

export interface IScream {
  screamId: string;
  body: string;
  createdAt: string;
  userHandle: string;
  userImage: string;
  likeCount: number;
  commentCount: number;
  comments: IComment[];
}

export interface IDataState {
  readonly screams: IScream[];
  scream?: IScream;
  readonly loading: boolean;
  readonly staticUserProfile?: IStaticUserProfile;
}

export interface IGetAllScreamsAction {
  type: DataReducerActionTypes.SET_SCREAMS;
  screams: IScream[];
}

export interface IDataLoadingAction {
  type: DataReducerActionTypes.LOADING_DATA;
}

export interface ILikeScreamAction {
  type: DataReducerActionTypes.LIKE_SCREAM;
  scream: IScream;
}

export interface IUnlikeScreamAction {
  type: DataReducerActionTypes.UNLIKE_SCREAM;
  scream: IScream;
}

export interface IDeleteScreamAction {
  type: DataReducerActionTypes.DELETE_SCREAM;
  screamId: string;
}

export interface IPostScreamAction {
  type: DataReducerActionTypes.POST_SCREAM;
  scream: IScream;
}

export interface IGetScreamAction {
  type: DataReducerActionTypes.GET_SCREAM;
  scream: IScream;
}

export interface ICommentAction {
  type: DataReducerActionTypes.POST_COMMENT;
  comment: IComment;
}

export interface IStaticUserProfile {
  screams: IScream[];
  user: IUser;
}

export interface IGetUserProfileAction {
  type: DataReducerActionTypes.GET_USER_PROFILE;
  staticUserProfile: IStaticUserProfile;
}

export interface IClearScreamAction {
  type: DataReducerActionTypes.CLEAR_SCREAM;
}

export type DataActions =
  | IGetAllScreamsAction
  | IDataLoadingAction
  | ILikeScreamAction
  | IUnlikeScreamAction
  | IDeleteScreamAction
  | IPostScreamAction
  | IGetScreamAction
  | ICommentAction
  | IClearScreamAction
  | IGetUserProfileAction;
