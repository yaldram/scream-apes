import { History } from "history";
import { IScream } from "./dataTypes";

export enum UserActionTypes {
  SET_AUTHENTICATED = "SET_AUTHENTICATED",
  SET_UNAUTHENTICATED = "SET_UNAUTHENTICATED",
  SET_USER = "SET_USER",
  LOADING_USER = "LOADING_USER",
  LIKE_SCREAM = "LIKE_SCREAM",
  UNLIKE_SCREAM = "UNLIKE_SCREAM",
  MARK_NOTIFICATIONS_READ = "MARK_NOTIFICATIONS_READ"
}

export interface IUserDetials {
  email: string;
}

export interface IGetUserDetialsAction {
  type: UserActionTypes.SET_USER;
  userInfo: {
    userDetails: IUser;
    likes: ILikeUnlikeScream[];
    notifications: INotifications[];
  };
}

export interface ISetAuthenticatedUserAction {
  type: UserActionTypes.SET_AUTHENTICATED;
}

export interface IUnsetAuthenticatedAction {
  type: UserActionTypes.SET_UNAUTHENTICATED;
}

export interface ILoadingUserAction {
  type: UserActionTypes.LOADING_USER;
}

export interface ILikeScreamUserAction {
  type: UserActionTypes.LIKE_SCREAM;
  scream: IScream;
}

export interface IUnlikeScreamAction {
  type: UserActionTypes.UNLIKE_SCREAM;
  scream: IScream;
}

export interface ILikeUnlikeScreamUserAction {
  type: UserActionTypes.UNLIKE_SCREAM;
  scream: IScream;
}

export interface ILoginData {
  email: string;
  password: string;
}

export interface ISignUpData {
  email: string;
  password: string;
  confirmPassword: string;
  handle: string;
}

export interface ILoginActionParams {
  loginData: ILoginData;
  history: History;
}

export interface ISignUpActionParams {
  signUpData: ISignUpData;
  history: History;
}

export interface IUser {
  handle: string;
  imageUrl: string;
  bio: string;
  website: string;
  location: string;
  createdAt: string;
  likes: ILikeUnlikeScream[];
}

export interface IUserProfile {
  bio: string;
  website: string;
  location: string;
}

export interface ILikeUnlikeScream {
  userHandle: string;
  screamId: string;
}

export interface INotifications {
  notificationId: string;
  read: boolean;
  readonly type: string;
  createdAt: string;
  recipient: string;
  screamId: string;
  sender: string;
}

export interface IMarkNotificationsAction {
  type: UserActionTypes.MARK_NOTIFICATIONS_READ;
}

export interface IUserState {
  readonly userCredentials?: IUser;
  readonly authenticated: boolean;
  readonly loading: boolean;
  readonly likes: ILikeUnlikeScream[];
  readonly notifications: INotifications[];
}

export type UserActions =
  | IGetUserDetialsAction
  | ISetAuthenticatedUserAction
  | IUnsetAuthenticatedAction
  | ILoadingUserAction
  | ILikeScreamUserAction
  | IUnlikeScreamAction
  | IMarkNotificationsAction;
