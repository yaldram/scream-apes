import * as React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

// Material UI
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";

import { IInitialState } from "../redux/store";
import { likeScream, unlikeScream } from "../redux/actions/dataActions";
import { IconButton } from "./IconButton";

interface ILikeButtonProps {
  screamId: string;
  likeCount?: number;
}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type LikeButtonProps = StateProps & DispatchProps & ILikeButtonProps;

class LikeButton extends React.Component<LikeButtonProps> {
  isScreamLiked = () => {
    const {
      user: { likes },
      screamId
    } = this.props;
    const isScreamLiked = likes.find(like => like.screamId === screamId);
    if (likes && isScreamLiked) {
      return true;
    }
    return false;
  };

  likeScream = () => {
    const { likeScream, screamId } = this.props;
    likeScream(screamId);
  };

  unlikeScream = () => {
    const { unlikeScream, screamId } = this.props;
    unlikeScream(screamId);
  };

  redirectLoginButton = () => (
    <Link to="/login">
      <IconButton tip="Like">
        <FavoriteBorder color="primary" />
      </IconButton>
    </Link>
  );

  renderLikeAndUnLikeButton = () => {
    if (!this.isScreamLiked()) {
      return (
        <IconButton tip="Like Scream" onClick={this.likeScream}>
          <FavoriteBorder color="primary" />
        </IconButton>
      );
    }
    return (
      <IconButton tip="UnLike Scream" onClick={this.unlikeScream}>
        <FavoriteIcon color="primary" />
      </IconButton>
    );
  };

  render() {
    const {
      user: { authenticated },
      likeCount
    } = this.props;
    if (!authenticated) {
      return this.redirectLoginButton();
    } else {
      return (
        <>
          {this.renderLikeAndUnLikeButton()}
          {likeCount && <span> {likeCount} Likes</span>}
        </>
      );
    }
  }
}

const mapStateToProps = ({ user }: IInitialState) => ({
  user
});

const mapDispatchToProps = (dispatch: any) => ({
  likeScream: (screamId: string) => dispatch(likeScream(screamId)),
  unlikeScream: (screamId: string) => dispatch(unlikeScream(screamId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LikeButton);
