import * as React from "react";
import { connect } from "react-redux";

import { WithStyles, withStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import EditIcon from "@material-ui/icons/Edit";

import { IInitialState } from "../redux/store";
import { IUserProfile } from "../redux/types/userTypes";
import { editProfile } from "../redux/actions/userActions";
import { IconButton } from "./IconButton";

const styles = {
  textField: {
    margin: "10px auto 10px auto"
  },
  button: {
    marignTop: 20
  }
};

interface IEditDetailsState {
  bio: string;
  website: string;
  location: string;
  open: boolean;
}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StyleProps = WithStyles<typeof styles>;
type EditDetialsProps = StateProps & StyleProps & DispatchProps;

class EditDetials extends React.Component<EditDetialsProps, IEditDetailsState> {
  state: IEditDetailsState = {
    bio: "",
    website: "",
    location: "",
    open: false
  };
  mapUserDetailsToState = () => {
    const { userCredentials: userProfile } = this.props;
    if (!userProfile) return;
    const { bio, website, location } = userProfile;
    this.setState({
      bio: bio ? bio : "",
      website: website ? website : "",
      location: location ? location : ""
    });
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [event.target.name]: event.target.value
    } as Pick<IEditDetailsState, any>);
  };

  handleOpen = () => {
    this.setState(
      {
        open: true
      },
      () => this.mapUserDetailsToState()
    );
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleSubmit = () => {
    const { bio, website, location } = this.state;
    const userDetails = { bio, website, location };
    this.props.editProfile(userDetails);
    this.handleClose();
  };

  componentDidMount() {
    this.mapUserDetailsToState();
  }

  render() {
    const { classes } = this.props;
    return (
      <>
        <IconButton
          tip="Edit Details"
          onClick={this.handleOpen}
          btnClassName={classes.button}
        >
          <EditIcon color="primary" />
        </IconButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <DialogTitle>Edit your details</DialogTitle>
          <DialogContent>
            <form>
              <TextField
                name="bio"
                type="text"
                label="Bio"
                multiline
                rows="3"
                placeholder="A short bio about yourself"
                className={classes.textField}
                value={this.state.bio}
                onChange={this.handleChange}
                fullWidth
              />
              <TextField
                name="website"
                type="text"
                label="Website"
                placeholder="Your personal/professinal website"
                className={classes.textField}
                value={this.state.website}
                onChange={this.handleChange}
                fullWidth
              />
              <TextField
                name="location"
                type="text"
                label="Location"
                placeholder="Where you live"
                className={classes.textField}
                value={this.state.location}
                onChange={this.handleChange}
                fullWidth
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

const mapStateToProps = ({ user: { userCredentials } }: IInitialState) => ({
  userCredentials
});

const mapDispatchToProps = (dispatch: any) => ({
  editProfile: (profile: IUserProfile) => dispatch(editProfile(profile))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(EditDetials));
