import * as React from "react";
import { connect } from "react-redux";

// MUI Stuff
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { deleteScream } from "../redux/actions/dataActions";
import { IconButton } from "./IconButton";

const styles = {
  deleteButton: {
    left: "90%",
    top: "10%"
  }
};

interface IDeleteScreamState {
  open: boolean;
}

interface IDeleteScreamProps {
  screamId: string;
}

type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StyleProps = WithStyles<typeof styles>;
type DeleteScreamProps = DispatchProps & StyleProps & IDeleteScreamProps;

class DeleteScream extends React.Component<
  DeleteScreamProps,
  IDeleteScreamState
> {
  state: IDeleteScreamState = {
    open: false
  };

  handleCloseDialog = () => {
    this.setState({ open: false });
  };

  handleOpenDialog = () => {
    this.setState({ open: true });
  };

  handleDeleteScream = () => {
    const { screamId, deleteScream } = this.props;
    deleteScream(screamId);
  };

  render() {
    const { classes } = this.props;

    return (
      <>
        <IconButton
          tip="Delete Scream"
          onClick={this.handleOpenDialog}
          btnClassName={classes.deleteButton}
          style={{ position: "absolute" }}
        >
          <DeleteOutline color="secondary" />
        </IconButton>

        <Dialog
          open={this.state.open}
          onClose={this.handleCloseDialog}
          fullWidth
          maxWidth="sm"
        >
          <DialogTitle>
            Are you sure you want to delete this scream ?
          </DialogTitle>
          <DialogActions>
            <Button onClick={this.handleCloseDialog} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleDeleteScream} color="secondary">
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  deleteScream: (screamId: string) => dispatch(deleteScream(screamId))
});

export default connect(
  null,
  mapDispatchToProps
)(withStyles(styles)(DeleteScream));
