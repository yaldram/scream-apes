import * as React from "react";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { connect } from "react-redux";

// Material UI
import NotificationsIcon from "@material-ui/icons/Notifications";
import FavoriteIcon from "@material-ui/icons/Favorite";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import ChatIcon from "@material-ui/icons/Chat";
import Badge from "@material-ui/core/Badge";
import { IInitialState } from "../redux/store";
import { markNotifications } from "../redux/actions/userActions";
import { INotifications } from "../redux/types/userTypes";
import { LinkTypoGraphy } from "./Scream";

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;

type NotificationProps = StateProps & DispatchProps;

interface INotificationState {
  anchorEl: any;
}

class Notifications extends React.Component<
  NotificationProps,
  INotificationState
> {
  state: INotificationState = {
    anchorEl: null
  };

  handleOpen = (event: React.SyntheticEvent) => {
    this.setState({ anchorEl: event.target });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  onMenuOpen = () => {
    const { notifications, markNotifications } = this.props;
    const unreadNotificationIds = notifications
      .filter(({ read }) => !read)
      .map(({ notificationId }) => notificationId);
    if (unreadNotificationIds.length) {
      markNotifications(unreadNotificationIds);
    }
  };

  renderNotificationIcon = () => {
    const { notifications } = this.props;
    if (!notifications.length) {
      return <NotificationsIcon />;
    }

    const unReadNotificationLength = notifications.filter(
      ({ read }) => read === false
    ).length;
    if (!unReadNotificationLength) return <NotificationsIcon />;
    return (
      <Badge badgeContent={unReadNotificationLength} color="secondary">
        <NotificationsIcon />
      </Badge>
    );
  };

  renderNotificationMarkup = ({
    createdAt,
    type,
    recipient,
    screamId,
    sender,
    read
  }: INotifications) => {
    const verb = type === "like" ? "liked" : "commented on";
    const time = dayjs(createdAt).fromNow();
    const iconColor = read ? "primary" : "secondary";
    const icon =
      type === "like" ? (
        <FavoriteIcon color={iconColor} style={{ marginRight: 10 }} />
      ) : (
        <ChatIcon color={iconColor} style={{ marginRight: 10 }} />
      );
    return (
      <MenuItem key={createdAt} onClick={this.handleClose}>
        {icon}
        <LinkTypoGraphy
          variant="body1"
          color="default"
          to={`/users/${recipient}/scream/${screamId}`}
        >
          {sender} {verb} your scream {time}
        </LinkTypoGraphy>
      </MenuItem>
    );
  };

  renderNotifications = () => {
    const { notifications } = this.props;
    if (!notifications.length) {
      return (
        <MenuItem onClick={this.handleClose}>
          You have no notifications yet
        </MenuItem>
      );
    }

    return notifications.map(notification =>
      this.renderNotificationMarkup(notification)
    );
  };

  render() {
    const { anchorEl } = this.state;
    dayjs.extend(relativeTime);
    return (
      <>
        <Tooltip placement="top" title="Notifications">
          <IconButton
            aria-owns={anchorEl && "simple-menu"}
            aria-haspopup="true"
            onClick={this.handleOpen}
          >
            {this.renderNotificationIcon()}
          </IconButton>
        </Tooltip>
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          onEntered={this.onMenuOpen}
        >
          {this.renderNotifications()}
        </Menu>
      </>
    );
  }
}

const mapStateToProps = ({ user: { notifications } }: IInitialState) => ({
  notifications
});

const mapDispatchToProps = (dispatch: any) => ({
  markNotifications: (notificationIds: string[]) =>
    dispatch(markNotifications(notificationIds))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notifications);
