import * as React from "react";
import { connect } from "react-redux";

// Material UI
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import CircularProgress from "@material-ui/core/CircularProgress";
import AddIcon from "@material-ui/icons/Add";
import CloseIcon from "@material-ui/icons/Close";
import { Theme, createStyles, WithStyles, withStyles } from "@material-ui/core";

import { IInitialState } from "../redux/store";
import { postScream, clearErrors } from "../redux/actions/dataActions";
import { IScream } from "../redux/types/dataTypes";
import { IconButton } from "./IconButton";

const styles = (theme: Theme) =>
  createStyles({
    submitButton: {
      position: "relative",
      float: "right",
      marginTop: 10
    },
    progressSpinner: {
      position: "absolute"
    },
    textField: {
      margin: "10px auto 10px auto"
    },
    closeButton: {
      position: "absolute",
      left: "91%",
      top: "6%"
    }
  });

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StyleProps = WithStyles<typeof styles>;

type AddScreamProps = StateProps & DispatchProps & StyleProps;

interface IAddScreamState {
  open: boolean;
  body: string;
}

class AddScream extends React.Component<AddScreamProps, IAddScreamState> {
  state: IAddScreamState = {
    open: false,
    body: ""
  };

  componentDidUpdate(prevProps: AddScreamProps) {
    const {
      UI: { loading: prevLoading }
    } = prevProps;
    const {
      UI: { loading }
    } = this.props;
    if (prevLoading !== loading && !loading) {
      this.setState({ open: false, body: "" });
    }
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ body: event.target.value });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleSubmit = (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
    this.props.postScream(this.state.body);
  };

  render() {
    const {
      classes,
      UI: { loading, errors }
    } = this.props;
    return (
      <>
        <IconButton tip="Post a Scream" onClick={this.handleOpen}>
          <AddIcon />
        </IconButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <IconButton
            tip="Close"
            onClick={this.handleClose}
            tipClassName={classes.closeButton}
          >
            <CloseIcon />
          </IconButton>
          <DialogTitle>Post a new scream</DialogTitle>
          <DialogContent>
            <form onSubmit={this.handleSubmit}>
              <TextField
                name="body"
                type="text"
                label="SCREAM!!"
                multiline
                rows="3"
                placeholder="Scream at your fellow apes"
                error={errors ? true : false}
                className={classes.textField}
                onChange={this.handleChange}
                fullWidth
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submitButton}
                disabled={loading}
              >
                Submit
                {loading && (
                  <CircularProgress
                    size={30}
                    className={classes.progressSpinner}
                  />
                )}
              </Button>
            </form>
          </DialogContent>
        </Dialog>
      </>
    );
  }
}

const mapStateToProps = ({ UI }: IInitialState) => ({
  UI
});

const mapDispatchToProps = (dispatch: any) => ({
  postScream: (screamBody: string) => dispatch(postScream(screamBody)),
  clearErrors: () => dispatch(clearErrors())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(AddScream));
