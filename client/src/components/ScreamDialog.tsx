import * as React from "react";
import { connect } from "react-redux";
import dayjs from "dayjs";
import { Link } from "react-router-dom";

// Material UI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import UnfoldMore from "@material-ui/icons/UnfoldMore";
import ChatIcon from "@material-ui/icons/Chat";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { Theme, createStyles } from "@material-ui/core";
import { IInitialState } from "../redux/store";
import {
  getScream,
  clearErrors,
  clearLoadedScream
} from "../redux/actions/dataActions";
import { IScream } from "../redux/types/dataTypes";
import LikeButton from "./Like";
import { IconButton } from "./IconButton";
import CommentForm from "./CommentForm";
import Comments from "./Comments";

const styles = (theme: Theme) =>
  createStyles({
    profileImage: {
      maxWidth: 200,
      height: 200,
      borderRadius: "50%",
      objectFit: "cover"
    },
    dialogContent: {
      padding: 20
    },
    invisibleSeparator: {
      border: "none",
      margin: 4
    },
    visibleSeparator: {
      width: "100%",
      borderBottom: "1px solid rgba(0,0,0,0.1)",
      marginBottom: 20
    },
    closeButton: {
      position: "absolute",
      left: "90%"
    },
    expandButton: {
      left: "90%"
    },
    spinnerDiv: {
      textAlign: "center",
      marginTop: 50,
      marginBottom: 50
    }
  });

interface IScreamDialogState {
  open: boolean;
  oldPath: string;
  newPath: string;
}

interface IScreamDialogProps {
  openDialog?: boolean;
  screamId: string;
  userHandle: string;
}

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StyleProps = WithStyles<typeof styles>;
type ScreamDialogProps = IScreamDialogProps &
  StateProps &
  DispatchProps &
  StyleProps;

class ScreamDialog extends React.Component<
  ScreamDialogProps,
  IScreamDialogState
> {
  state: IScreamDialogState = {
    open: false,
    oldPath: "",
    newPath: ""
  };

  componentDidMount() {
    if (this.props.openDialog) {
      this.handleOpen();
    }
  }

  handleOpen = () => {
    let oldPath = window.location.pathname;

    const { userHandle, screamId, getScream } = this.props;
    const newPath = `/users/${userHandle}/scream/${screamId}`;

    if (oldPath === newPath) oldPath = `/users/${userHandle}`;

    window.history.pushState(null, "", newPath);

    this.setState({ open: true, oldPath, newPath });
    getScream(screamId);
  };

  handleClose = () => {
    window.history.pushState(null, "", this.state.oldPath);
    this.setState({ open: false });
    this.props.clearScream();
    this.props.clearErrors();
  };

  renderDialogGrid = (scream?: IScream) => {
    if (!scream) {
      return null;
    }
    const {
      screamId,
      body,
      createdAt,
      likeCount,
      commentCount,
      userHandle,
      userImage,
      comments
    } = scream;
    const { classes } = this.props;
    return (
      <Grid container spacing={16}>
        <Grid item sm={5}>
          <img src={userImage} alt="Profile" className={classes.profileImage} />
        </Grid>
        <Grid item sm={7}>
          <Typography
            component={({ innerRef, ...props }) => (
              <Link {...props} to={`/users/${userHandle}`} />
            )}
            color="primary"
            variant="h5"
          >
            @{userHandle}
          </Typography>
          <hr className={classes.invisibleSeparator} />
          <Typography variant="body2" color="textSecondary">
            {dayjs(createdAt).format("h:mm a, MMMM DD YYYY")}
          </Typography>
          <hr className={classes.invisibleSeparator} />
          <Typography variant="body1">{body}</Typography>
          <LikeButton screamId={screamId} />
          <span>{likeCount} likes</span>
          <IconButton tip="comments">
            <ChatIcon color="primary" />
          </IconButton>
          <span>{commentCount} comments</span>
        </Grid>
        <hr className={classes.visibleSeparator} />
        <CommentForm screamId={screamId} />
        <Comments comments={comments} />
      </Grid>
    );
  };

  renderDialog = () => {
    const {
      classes,
      UI: { loading },
      scream
    } = this.props;
    if (!scream && loading) {
      return (
        <div className={classes.spinnerDiv}>
          <CircularProgress size={200} thickness={2} />
        </div>
      );
    }
    return this.renderDialogGrid(scream);
  };

  render() {
    const { classes } = this.props;
    return (
      <>
        <IconButton
          style={{ position: "absolute" }}
          onClick={this.handleOpen}
          tip="Expand scream"
          tipClassName={classes.expandButton}
        >
          <UnfoldMore color="primary" />
        </IconButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <IconButton
            tip="Close"
            onClick={this.handleClose}
            tipClassName={classes.closeButton}
          >
            <CloseIcon />
          </IconButton>
          <DialogContent className={classes.dialogContent}>
            {this.renderDialog()}
          </DialogContent>
        </Dialog>
      </>
    );
  }
}

const mapStateToProps = ({ data: { scream }, UI }: IInitialState) => ({
  scream,
  UI
});

const mapDispatchToProps = (dispatch: any) => ({
  getScream: (screamId: string) => dispatch(getScream(screamId)),
  clearErrors: () => dispatch(clearErrors()),
  clearScream: () => dispatch(clearLoadedScream())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(ScreamDialog));
