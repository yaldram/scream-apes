import React from "react";
import { Link } from "react-router-dom";

import Button, { ButtonProps } from "@material-ui/core/Button";

interface ICustomLinkButtonProps extends ButtonProps {
  to: string;
  buttonText: string;
}

export const CustomLinkButton: React.SFC<ICustomLinkButtonProps> = ({
  to,
  buttonText
}) => (
  <Button
    color="inherit"
    component={({ innerRef, ...props }) => <Link {...props} to={to} />}
  >
    {buttonText}
  </Button>
);
