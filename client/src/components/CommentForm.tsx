import * as React from "react";
import { connect } from "react-redux";

// Material UI
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import {
  Theme,
  createStyles,
  WithStyles,
  withStyles,
  Grid
} from "@material-ui/core";

import { IInitialState } from "../redux/store";
import { postComment } from "../redux/actions/dataActions";

const styles = (theme: Theme) =>
  createStyles({
    visibleSeparator: {
      width: "100%",
      borderBottom: "1px solid rgba(0,0,0,0.1)",
      marginBottom: 20
    },
    textField: {
      margin: "10px auto 10px auto"
    },
    button: {
      marignTop: 20
    }
  });

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StyleProps = WithStyles<typeof styles>;

interface ICommentFormProps {
  screamId: string;
}

type CommentFormProps = ICommentFormProps &
  StateProps &
  DispatchProps &
  StyleProps;

interface ICommentFormState {
  body: string;
}

class CommentForm extends React.Component<CommentFormProps, ICommentFormState> {
  state: ICommentFormState = {
    body: ""
  };
  componentDidUpdate(prevProps: CommentFormProps) {
    const {
      UI: { loading: prevLoading }
    } = prevProps;
    const {
      UI: { loading }
    } = this.props;

    if (prevLoading !== loading && !loading) {
      this.setState({ body: "" });
    }
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ body: event.target.value });
  };

  handleSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault();
    const { postComment, screamId } = this.props;
    const { body } = this.state;
    postComment(screamId, { body });
  };

  renderForm = () => {
    const {
      UI: { errors },
      classes
    } = this.props;

    return (
      <Grid item sm={12} style={{ textAlign: "center" }}>
        <form onSubmit={this.handleSubmit}>
          <TextField
            name="body"
            type="text"
            label="Comment on scream"
            error={errors ? true : false}
            helperText={errors}
            value={this.state.body}
            onChange={this.handleChange}
            fullWidth
            className={classes.textField}
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Submit
          </Button>
        </form>
        <hr className={classes.visibleSeparator} />
      </Grid>
    );
  };

  render() {
    const { authenticated } = this.props;
    return <>{authenticated && this.renderForm()}</>;
  }
}

const mapStateToProps = (state: IInitialState) => ({
  UI: state.UI,
  authenticated: state.user.authenticated
});

const mapDispatchToProps = (dispatch: any) => ({
  postComment: (screamId: string, commentBody: { body: string }) =>
    dispatch(postComment(screamId, commentBody))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(CommentForm));
