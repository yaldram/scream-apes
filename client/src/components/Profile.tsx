import * as React from "react";
import dayjs from "dayjs";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

// Material UI
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import MuiLink from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import { Theme, createStyles } from "@material-ui/core";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";

// Material UI Icons
import LocationOn from "@material-ui/icons/LocationOn";
import LinkIcon from "@material-ui/icons/Link";
import CalendarToday from "@material-ui/icons/CalendarToday";
import EditIcon from "@material-ui/icons/Edit";
import KeyboardReturn from "@material-ui/icons/KeyboardReturn";

import { logoutUser, uploadImage } from "../redux/actions/userActions";
import { IInitialState } from "../redux/store";
import { IconButton } from "./IconButton";
import EditDetials from "./EditDetials";
import ProfileSkeleton from "./ProfileSkeleton";

const styles = (theme: Theme) =>
  createStyles({
    paper: {
      padding: 20
    },
    profile: {
      "& .image-wrapper": {
        textAlign: "center",
        position: "relative",
        "& button": {
          position: "absolute",
          top: "80%",
          left: "70%"
        }
      },
      "& .profile-image": {
        width: 200,
        height: 200,
        objectFit: "cover",
        maxWidth: "100%",
        borderRadius: "50%"
      },
      "& .profile-details": {
        textAlign: "center",
        "& span, svg": {
          verticalAlign: "middle"
        },
        "& a": {
          color: theme.palette.primary.main
        }
      },
      "& hr": {
        border: "none",
        margin: "0 0 10px 0"
      },
      "& svg.button": {
        "&:hover": {
          cursor: "pointer"
        }
      }
    },
    buttons: {
      textAlign: "center",
      "& a": {
        margin: "20px 10px"
      }
    }
  });

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type StyleProps = WithStyles<typeof styles>;

type ProfileProps = StateProps & DispatchProps & StyleProps;

class Profile extends React.Component<ProfileProps> {
  handleImageChange = (event: any) => {
    const image = event.target.files[0];
    const formData = new FormData();
    formData.append("image", image, image.name);
    this.props.uploadImage(formData);
  };

  handleEditPicture = () => {
    const fileInput = document.getElementById("imageInput");
    if (fileInput) {
      fileInput.click();
    }
  };

  renderLoginSignupButton = () => {
    const { classes } = this.props;
    return (
      <Paper className={classes.paper}>
        <Typography variant="body2" align="center">
          No profile found, please login again
        </Typography>
        <div className={classes.buttons}>
          <Button
            variant="contained"
            color="primary"
            component={({ innerRef, ...props }) => (
              <Link {...props} to="/login" />
            )}
          >
            Login
          </Button>
          <Button
            variant="contained"
            color="secondary"
            component={({ innerRef, ...props }) => (
              <Link {...props} to="/signup" />
            )}
          >
            Signup
          </Button>
        </div>
      </Paper>
    );
  };

  renderUserLocation = (location: string) => (
    <>
      <LocationOn color="primary" /> <span>{location}</span>
      <hr />
    </>
  );

  renderUserWebsite = (website: string) => (
    <>
      <LinkIcon color="primary" />
      <a href={website} target="_blank" rel="noopener noreferrer">
        {" "}
        {website}
      </a>
      <hr />
    </>
  );

  renderSkeleton = () => <ProfileSkeleton />;

  renderProfileInfo = () => {
    if (!this.props.userCredentials) {
      return null;
    }
    const {
      userCredentials: { handle, imageUrl, website, location, bio, createdAt },
      classes
    } = this.props;
    return (
      <Paper className={classes.paper}>
        <div className={classes.profile}>
          <div className="image-wrapper">
            <img src={imageUrl} alt="profile" className="profile-image" />
            <input
              type="file"
              id="imageInput"
              hidden
              onChange={this.handleImageChange}
            />
            <IconButton
              tip="Edit profile picture"
              onClick={this.handleEditPicture}
              btnClassName="button"
            >
              <EditIcon color="primary" />
            </IconButton>
          </div>
          <hr />
          <div className="profile-details">
            <MuiLink
              component={({ innerRef, ...props }) => (
                <Link {...props} to={`/users/${handle}`} />
              )}
              color="primary"
              variant="h5"
            >
              {" "}
              @{handle}{" "}
            </MuiLink>
            <hr />
            {bio && <Typography variant="body2">{bio}</Typography>}
            <hr />
            {location && this.renderUserLocation(location)}
            {website && this.renderUserWebsite(website)}
            <CalendarToday color="primary" />{" "}
            <span>Joined {dayjs(createdAt).format("MMM YYYY")}</span>
          </div>
          <IconButton tip="Logout" onClick={() => this.props.logoutUser()}>
            <KeyboardReturn color="primary" />
          </IconButton>
          <EditDetials />
        </div>
      </Paper>
    );
  };

  render() {
    const { loading, authenticated } = this.props;
    return (
      <>
        {loading
          ? this.renderSkeleton()
          : authenticated
          ? this.renderProfileInfo()
          : this.renderLoginSignupButton()}
      </>
    );
  }
}

const mapStateToProps = ({
  user: { authenticated, userCredentials, loading }
}: IInitialState) => ({
  loading,
  authenticated,
  userCredentials
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    uploadImage: (formData: FormData) => dispatch(uploadImage(formData)),
    logoutUser: () => dispatch(logoutUser())
  };
};

// export const Profile: React.ComponentClass<{}> = connect(mapDispatchToProps)
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Profile));
