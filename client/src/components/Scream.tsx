import * as React from "react";
import dayjs from "dayjs";
import { connect } from "react-redux";
import relativeTime from "dayjs/plugin/relativeTime";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { Link } from "react-router-dom";

// Material UI Components
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography, { TypographyProps } from "@material-ui/core/Typography";

import LikeButton from "./Like";
import { IScream } from "../redux/types/dataTypes";
import { IInitialState } from "../redux/store";
import DeleteButton from "./DeleteButton";
import ScreamDialog from "./ScreamDialog";

interface IScreamProps {
  scream: IScream;
  openDialog?: boolean;
}

interface LinkTypoGraphyProps extends TypographyProps {
  to: string;
  replace?: boolean;
}

type StateProps = ReturnType<typeof mapStateToProps>;
type StyleProps = WithStyles<typeof styles>;
type ScreamProps = StateProps & StyleProps & IScreamProps;

export const LinkTypoGraphy = (props: LinkTypoGraphyProps) => (
  <Typography {...props} component={Link as any} />
);

class Scream extends React.PureComponent<ScreamProps, {}> {
  renderDeleteButton = () => {
    const {
      user: { authenticated, userCredentials },
      scream: { userHandle, screamId }
    } = this.props;
    if (!userCredentials) {
      return;
    }

    if (authenticated && userCredentials.handle === userHandle) {
      return <DeleteButton screamId={screamId} />;
    }
  };

  render() {
    dayjs.extend(relativeTime);
    const {
      classes,
      scream: { screamId, body, createdAt, userHandle, userImage, likeCount },
      openDialog
    } = this.props;

    return (
      <Card className={classes.card} key={screamId}>
        <CardMedia
          image={userImage}
          title="Profile Image"
          className={classes.image}
        />
        <CardContent className={classes.content}>
          <LinkTypoGraphy
            variant="h5"
            color="primary"
            to={`/users/${userHandle}`}
          >
            {userHandle}
          </LinkTypoGraphy>
          <Typography variant="body2" color="textSecondary">
            {dayjs(createdAt).fromNow()}
          </Typography>
          <Typography variant="body1">{body}</Typography>
          <LikeButton screamId={screamId} likeCount={likeCount} />
          {this.renderDeleteButton()}
          <ScreamDialog
            screamId={screamId}
            userHandle={userHandle}
            openDialog={openDialog}
          />
        </CardContent>
      </Card>
    );
  }
}

const styles = {
  card: {
    display: "flex",
    marginBottom: 20
  },
  image: {
    minWidth: 200
  },
  content: {
    padding: 25
  }
};

const mapStateToProps = ({ user }: IInitialState) => ({
  user
});

export default connect(mapStateToProps)(withStyles(styles)(Scream));
