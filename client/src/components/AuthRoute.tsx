import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { IInitialState } from "../redux/store";

export interface IAuthRouteProps {
  component: React.ComponentType<any>;
  exact: boolean;
  path: string;
  authenticated: boolean;
}

const AuthRoute: React.FunctionComponent<IAuthRouteProps> = ({
  component: Component,
  authenticated,
  ...rest
}) => (
  <Route
    {...rest}
    render={props =>
      authenticated === true ? <Redirect to="/" /> : <Component {...props} />
    }
  />
);

const mapStateToProps = ({ user: { authenticated } }: IInitialState) => ({
  authenticated
});

export default connect(mapStateToProps)(AuthRoute);
