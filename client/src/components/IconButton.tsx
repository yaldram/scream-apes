import React from "react";

import Tooltip from "@material-ui/core/Tooltip";
import {
  default as MUIconButton,
  IconButtonProps as MUIconButtonProps
} from "@material-ui/core/IconButton";

interface IconButtonProps extends MUIconButtonProps {
  tip: string;
  btnClassName?: string;
  tipClassName?: string;
}

export const IconButton: React.SFC<IconButtonProps> = ({
  children,
  onClick,
  tip,
  btnClassName,
  tipClassName
}) => (
  <Tooltip title={tip} className={tipClassName} placement="top">
    <MUIconButton onClick={onClick} className={btnClassName}>
      {children}
    </MUIconButton>
  </Tooltip>
);
