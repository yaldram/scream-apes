import * as React from "react";
import dayjs from "dayjs";
import { Link } from "react-router-dom";

// Material UI
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import { Theme, createStyles } from "@material-ui/core";
import { IComment } from "../redux/types/dataTypes";

const styles = (theme: Theme) =>
  createStyles({
    commentImage: {
      maxWidth: "100%",
      height: 100,
      objectFit: "cover",
      borderRadius: "50%"
    },
    invisibleSeparator: {
      border: "none",
      margin: 4
    },
    commentData: {
      marginLeft: 20
    },
    visibleSeparator: {
      width: "100%",
      borderBottom: "1px solid rgba(0,0,0,0.1)",
      marginBottom: 20
    }
  });

type StyleProps = WithStyles<typeof styles>;

interface ICommentProps {
  comments: IComment[];
}

class Comment extends React.Component<ICommentProps & StyleProps> {
  render() {
    const { comments, classes } = this.props;
    return (
      <Grid container>
        {comments.map((comment, index) => {
          const { body, createdAt, userHandle, userImage } = comment;
          return (
            <React.Fragment key={createdAt}>
              <Grid item sm={12}>
                <Grid container>
                  <Grid item sm={2}>
                    <img
                      src={userImage}
                      alt="comment"
                      className={classes.commentImage}
                    />
                  </Grid>
                  <Grid item sm={9}>
                    <div className={classes.commentData}>
                      <Typography
                        variant="h5"
                        component={({ innerRef, ...props }) => (
                          <Link {...props} to={`/users/${userHandle}`} />
                        )}
                        color="primary"
                      >
                        {userHandle}
                      </Typography>
                      <Typography variant="body2" color="textSecondary">
                        {dayjs(createdAt).format("h:mm a, MMMM DD YYYY")}
                      </Typography>
                      <hr className={classes.invisibleSeparator} />
                      <Typography variant="body1">{body}</Typography>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
              {index !== comments.length - 1 && (
                <hr className={classes.visibleSeparator} />
              )}
            </React.Fragment>
          );
        })}
      </Grid>
    );
  }
}

export default withStyles(styles)(Comment);
