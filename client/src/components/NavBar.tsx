import * as React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

// Material UI Components
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";

import { CustomLinkButton } from "./CustomLinkButton";
import { IInitialState } from "../redux/store";
import { IconButton } from "./IconButton";
import AddScream from "./AddScream";
import Notifications from "./Notifications";

type StateProps = ReturnType<typeof mapStateToProps>;
type NavBarProps = StateProps;

class NavBar extends React.Component<NavBarProps> {
  notAuthenticatedUi = () => (
    <>
      <CustomLinkButton to="/" buttonText="Home" />
      <CustomLinkButton to="/signup" buttonText="SignUp" />
      <CustomLinkButton to="/login" buttonText="Login" />
    </>
  );

  authenticatedUi = () => (
    <>
      <AddScream />
      <Link to="/">
        <IconButton tip="Home">
          <HomeIcon />
        </IconButton>
      </Link>
      <Notifications />
    </>
  );

  renderUi = () => {
    if (this.props.authenticated) {
      return this.authenticatedUi();
    }
    return this.notAuthenticatedUi();
  };

  render() {
    return (
      <AppBar>
        <Toolbar className="nav-container">{this.renderUi()}</Toolbar>
      </AppBar>
    );
  }
}

const mapStateToProps = ({ user: { authenticated } }: IInitialState) => ({
  authenticated
});

export default connect(mapStateToProps)(NavBar);
