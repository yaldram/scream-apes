import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import axios from "axios";
import jwtDecode from "jwt-decode";

// Bring in our pages
import Home from "./pages/Home";
import Login from "./pages/Login";
import SignUp from "./pages/Signup";
import User from "./pages/User";

import AuthRoute from "./components/AuthRoute";

// Material Ui Theme
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

// Bring in our components
import NavBar from "./components/NavBar";
import store from "./redux/store";
import { logoutUser, getUserData } from "./redux/actions/userActions";
import {
  ISetAuthenticatedUserAction,
  UserActionTypes
} from "./redux/types/userTypes";

axios.defaults.baseURL =
  "https://europe-west1-scream-apes.cloudfunctions.net/api";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#00bcd4",
      light: "#33c9dc",
      dark: "#008394",
      contrastText: "#fff"
    },
    secondary: {
      main: "#ff3d00",
      light: "#ff6333",
      dark: "#b22a00",
      contrastText: "#fff"
    }
  },
  typography: {
    useNextVariants: true
  }
});

const token = localStorage.FBIdToken;
if (token) {
  const decodedToken: { exp: number } = jwtDecode(token);
  if (decodedToken.exp * 1000 < Date.now()) {
    store.dispatch<any>(logoutUser());
    window.location.href = "/login";
  } else {
    store.dispatch<ISetAuthenticatedUserAction>({
      type: UserActionTypes.SET_AUTHENTICATED
    });
    axios.defaults.headers.common["Authorization"] = token;
    store.dispatch<any>(getUserData());
  }
}

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Provider store={store}>
          <BrowserRouter>
            <NavBar />
            <div className="container">
              <Switch>
                <Route exact path="/" component={Home} />
                <AuthRoute exact path="/login" component={Login} />
                <AuthRoute exact path="/signup" component={SignUp} />
                <Route exact path="/users/:handle" component={User} />
              </Switch>
            </div>
          </BrowserRouter>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
