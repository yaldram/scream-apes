import { Response, NextFunction } from "express";

import { firebaseAdmin as admin, db } from "../init";

export default async (req: any, res: Response, next: NextFunction) => {
  let idToken;
  try {
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith("Bearer")
    ) {
      idToken = req.headers.authorization.split(" ")[1];
    } else {
      console.warn("No Token Found");
      res.status(403).json({
        error: "UnAuthorized"
      });
      return;
    }

    const decodedToken = await admin.auth().verifyIdToken(idToken);
    req.user = decodedToken;
    const { docs } = await db
      .collection("users")
      .where("userId", "==", req.user.uid)
      .limit(1)
      .get();
    req.user.handle = docs[0].data().handle;
    req.user.imageUrl = docs[0].data().imageUrl;
    next();
  } catch (error) {
    console.warn("Error verifying Token", error);
    res.status(400).json({ code: "auth-failed", message: "Auth Failed" });
  }
};
