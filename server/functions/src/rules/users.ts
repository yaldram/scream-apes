import { check } from "express-validator/check";

export const userRules = {
  register: [
    check("email")
      .isEmail()
      .withMessage("Please Enter a valid email"),
    check("handle")
      .exists()
      .isLength({ min: 5 })
      .withMessage("User Handle must be atleast 5 characters"),
    check("password")
      .exists()
      .withMessage("Password is Required"),
    check(
      "confirmPassword",
      "Password Confirmation field must have the same value as password field"
    )
      .exists()
      .custom((value, { req }) => value === req.body.password)
  ],
  login: [
    check("email")
      .isEmail()
      .withMessage("Please Enter a valid Email"),
    check("password")
      .exists()
      .withMessage("Password field is required")
  ]
};
