import { check } from "express-validator/check";

export const screamRules = {
  insertScream: [
    check("body")
      .exists()
      .isLength({ min: 5 })
      .withMessage("Body is required")
  ],
  insertComment: [
    check("body")
      .exists()
      .withMessage("Body is required")
  ]
};
