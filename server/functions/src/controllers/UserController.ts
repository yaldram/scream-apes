import { Response } from "express";
import * as path from "path";
import * as os from "os";
import * as fs from "fs";
import * as BusBoy from "busboy";

import { firebaseAdmin as admin, db } from "../init";
import { config } from "../../config";
import { reduceUserDetails } from "../services/user";

class UserController {
  uploadImage = async (req: any, res: Response) => {
    try {
      const busboy = new BusBoy({ headers: req.headers });
      let imageFileName: string;
      let imageToBeUploaded: any = {};

      busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
        if (
          mimetype !== "image/jpg" &&
          mimetype !== "image/png" &&
          mimetype !== "image/jpeg"
        ) {
          res.status(400).json({
            message: "Please Upload a Photo"
          });
          return;
        }
        const imageExtension = filename.split(".")[
          filename.split(".").length - 1
        ];
        imageFileName = `${Math.round(Math.random() * 100)}.${imageExtension}`;
        const filepath = path.join(os.tmpdir(), imageFileName);
        imageToBeUploaded = { filepath, mimetype };
        file.pipe(fs.createWriteStream(filepath));
      });

      busboy.on("finish", async () => {
        await admin
          .storage()
          .bucket()
          .upload(imageToBeUploaded.filepath, {
            resumable: false,
            metadata: {
              metadata: {
                contentType: imageToBeUploaded.mimetype
              }
            }
          });
        const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageFileName}?alt=media`;
        await db.doc(`/users/${req.user.handle}`).update({ imageUrl });
        return res.json({ message: "Image Uploaded" });
      });

      busboy.end(req.rawBody);
    } catch (error) {
      console.warn("ERROR WHILE UPLOADING IMAGE", error);
      res.status(500).json({ error });
    }
  };

  addDetails = async (req: any, res: Response) => {
    try {
      const userDetails = reduceUserDetails(req.body);
      await db.doc(`/users/${req.user.handle}`).update(userDetails);
      return res.status(200).json({ message: "Details updated Successfully" });
    } catch (error) {
      console.log("Error in addDetails", error);
      return res.status(500).json({ error });
    }
  };

  getDetails = async (req: any, res: Response) => {
    try {
      const userData: any = {};
      const userDoc = await db.doc(`users/${req.user.handle}`).get();
      if (userDoc.exists) {
        userData.credentials = userDoc.data();
      }
      const likes = await db
        .collection("likes")
        .where("userHandle", "==", req.user.handle)
        .get();
      userData.likes = [];
      likes.forEach(like => {
        userData.likes.push(like.data());
      });
      const notifications = await db
        .collection("notifications")
        .where("recipient", "==", req.user.handle)
        .orderBy("createdAt", "desc")
        .limit(10)
        .get();
      userData.notifications = [];
      notifications.forEach(notification => {
        userData.notifications.push({
          ...notification.data(),
          notificationId: notification.id
        });
      });
      return res.status(200).json(userData);
    } catch (error) {
      console.log("Error in getDetails", error);
      return res.status(500).json({ error });
    }
  };

  getUserDetails = async (req: any, res: Response) => {
    const userData: any = {};
    try {
      const userDoc = await db.doc(`users/${req.params.handle}`).get();
      if (userDoc.exists) {
        userData.user = userDoc.data();
        const screamDoc = await db
          .collection("screams")
          .where("userHandle", "==", req.params.handle)
          .orderBy("createdAt", "desc")
          .get();
        userData.screams = [];
        screamDoc.forEach(scream => {
          userData.screams.push({
            ...scream.data(),
            screamId: scream.id
          });
        });
        return res.status(200).json(userData);
      }
      return res.status(404).json({ message: "User Does not Exists" });
    } catch (error) {
      console.log("Error in getUserDetails", error);
      return res.status(500).json({ error });
    }
  };

  markNotificationsRead = async (req: any, res: Response) => {
    try {
      const batch = db.batch();
      req.body.forEach((notificationId: any) => {
        const notification = db.doc(`notifications/${notificationId}`);
        batch.update(notification, { read: true });
      });
      await batch.commit();
      return res.status(200).json({ message: "Notifications read" });
    } catch (error) {
      console.log("Error in markNotifications", error);
      return res.status(500).json({ error });
    }
  };
}

export default new UserController();
