import { Request, Response } from "express";

import { firebaseInit as firebase, db } from "../init";
import { config } from "../../config";
import validate from "../services/validation";

class AuthController {
  signup = async (req: Request, res: Response) => {
    try {
      if (validate(req, res, "auth/signup-validation")) return;
      const { email, password, handle } = req.body;
      const userDoc = await db.doc(`/users/${handle}`).get();
      if (userDoc.exists) {
        res.status(400).json({
          handle: "User with given handle already exists"
        });
        return;
      }
      const data = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);
      if (data.user) {
        const token = await data.user.getIdToken();
        const defaultProfileImage = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/photo.jpg?alt=media`;
        await db
          .doc(`/users/${handle}`)
          .set({
            handle,
            email,
            createdAt: new Date().toISOString(),
            imageUrl: defaultProfileImage,
            userId: data.user.uid
          });
        res.status(201).json({
          token
        });
      }
    } catch (error) {
      console.error("Error occured while signing up", error);
      if (error.code === "auth/email-already-in-use") {
        res.status(400).json({
          email: "Email is already in use"
        });
        return;
      }
      res.status(500).json({ error: error.code });
    }
  };

  login = async (req: Request, res: Response) => {
    try {
      if (validate(req, res, "auth/login-validation")) return;
      const { email, password } = req.body;
      const data = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      if (data.user) {
        const token = await data.user.getIdToken();
        res.status(200).json({
          token
        });
      }
    } catch (error) {
      if (error.code === "auth/wrong-password") {
        res.status(400).json({
          password: "Wrong Credentials, please try again"
        });
        return;
      } else if (error.code === "auth/user-not-found") {
        res.status(400).json({
          email: "User with the given email does not exists"
        });
        return;
      }
      res.status(500).json(error);
    }
  };
}

export default new AuthController();
