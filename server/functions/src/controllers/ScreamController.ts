import { Request, Response } from "express";

import { db } from "../init";
import validate from "../services/validation";

class ScreamController {
  getScreams = async (req: Request, res: Response) => {
    try {
      const screamsData = await db
        .collection("screams")
        .orderBy("createdAt", "desc")
        .get();
      const screams: Array<FirebaseFirestore.DocumentData> = [];
      screamsData.forEach(doc => {
        screams.push({
          screamId: doc.id,
          ...doc.data()
        });
      });
      res.status(200).json(screams);
    } catch (error) {
      console.error("Error while fetching Screams", error);
      res.status(401).json(error);
    }
  };

  insertScream = async (req: any, res: Response) => {
    try {
      if (validate(req, res, "scream/insert-validation")) return;
      const { body } = req.body;
      const newScream = {
        body,
        createdAt: new Date().toISOString(),
        userImage: req.user.imageUrl,
        userHandle: req.user.handle,
        likeCount: 0,
        commentCount: 0
      };
      const scream = await db.collection("screams").add(newScream);
      const resScream = { ...newScream, screamId: scream.id };
      res.status(201).json({
        scream: resScream
      });
    } catch (error) {
      console.error("Error While Creating Scream", error);
      res.status(500).json(error);
    }
  };

  getScream = async (req: any, res: Response) => {
    try {
      let screamData: any = {};
      const screamDoc = await db.doc(`/screams/${req.params.screamId}`).get();
      if (!screamDoc.exists) {
        return res.status(404).json({ error: "Scream not Found" });
      }
      screamData = screamDoc.data();
      screamData.screamId = screamDoc.id;
      const commentsDoc = await db
        .collection("comments")
        .orderBy("createdAt", "desc")
        .where("screamId", "==", req.params.screamId)
        .get();
      screamData.comments = [];
      commentsDoc.forEach(doc => {
        screamData.comments.push(doc.data());
      });
      return res.status(200).json(screamData);
    } catch (error) {
      console.error("Error While Fetching Scream", error);
      return res.status(500).json(error);
    }
  };

  insertComment = async (req: any, res: Response) => {
    try {
      const newComment = {
        body: req.body.body,
        createdAt: new Date().toISOString(),
        screamId: req.params.screamId,
        userHandle: req.user.handle,
        userImage: req.user.imageUrl
      };

      const screamDoc = await db.doc(`/screams/${req.params.screamId}`).get();
      if (!screamDoc.exists) {
        return res.status(404).json({ message: "Scream Not Found" });
      }
      await screamDoc.ref.update({
        commentCount: screamDoc.data()!.commentCount + 1
      });
      await db.collection("comments").add(newComment);
      return res.status(201).json(newComment);
    } catch (error) {
      console.error("Error While Commenting on Scream", error);
      return res.status(500).json(error);
    }
  };

  like = async (req: any, res: Response) => {
    try {
      let screamData: any;
      const screamDocument = await db.doc(`screams/${req.params.screamId}`);
      const screamDoc = await screamDocument.get();
      if (!screamDoc.exists) {
        return res.status(404).json({ message: "Scream Not Found" });
      }
      const likeDoc = await db
        .collection("likes")
        .where("userHandle", "==", req.user.handle)
        .where("screamId", "==", req.params.screamId)
        .limit(1)
        .get();

      if (!likeDoc.empty) {
        return res.status(400).json({ message: "Scream Already Liked" });
      }

      screamData = { ...screamDoc.data(), screamId: screamDoc.id };
      await db.collection("likes").add({
        screamId: req.params.screamId,
        userHandle: req.user.handle
      });
      screamData.likeCount++;
      await screamDocument.update({ likeCount: screamData.likeCount });
      return res.status(200).json(screamData);
    } catch (error) {
      console.warn("Error While Liking Stream", error);
      return res.status(500).json({ error });
    }
  };

  unlike = async (req: any, res: Response) => {
    try {
      let screamData: any;
      const screamDocument = await db.doc(`screams/${req.params.screamId}`);
      const screamDoc = await screamDocument.get();
      if (!screamDoc.exists) {
        return res.status(404).json({ message: "Scream Not Found" });
      }
      const likeDoc = await db
        .collection("likes")
        .where("userHandle", "==", req.user.handle)
        .where("screamId", "==", req.params.screamId)
        .limit(1)
        .get();
      if (likeDoc.empty) {
        return res.status(400).json({ message: "Scream Not Liked" });
      }
      screamData = { ...screamDoc.data(), screamId: screamDoc.id };
      await db.doc(`likes/${likeDoc.docs[0].id}`).delete();
      screamData.likeCount--;
      await screamDocument.update({ likeCount: screamData.likeCount });
      return res.status(200).json(screamData);
    } catch (error) {
      console.warn("Error While UnLiking Stream", error);
      return res.status(500).json({ error });
    }
  };

  deleteScream = async (req: any, res: Response) => {
    try {
      const screamDoc = await db.doc(`screams/${req.params.screamId}`).get();
      if (!screamDoc.exists) {
        return res.status(404).json({ message: "Scream not found" });
      }
      if (screamDoc.data()!.userHandle !== req.user.handle) {
        return res.status(403).json({ message: "UnAuthorized" });
      }
      await screamDoc.ref.delete();
      return res.status(200).json({ message: "Scream Deleted Successfully" });
    } catch (error) {
      console.warn("Error While UnLiking Stream", error);
      return res.status(500).json({ error });
    }
  };
}

export default new ScreamController();
