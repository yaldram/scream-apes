import * as express from "express";
import * as cors from "cors";

import { screamRouter } from "./routes/scream.router";
import { authRouter } from "./routes/auth.router";
import { userRouter } from "./routes/user.router";

class App {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
    this.globalErrorHandler();
  }

  private config(): void {
    this.app.use(express.json());
    this.app.use(cors());
  }

  private routes(): void {
    this.app.use("/", authRouter);
    this.app.use("/", screamRouter);
    this.app.use("/user", userRouter);
  }

  private globalErrorHandler(): void {
    this.app.use(
      (
        error: Error,
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) => {
        console.log("ERROR", error.stack);
        res.status(500).send({
          message: "Something Unusual Happened"
        });
      }
    );
  }
}

export default new App().app;
