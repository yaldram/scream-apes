import { Router } from "express";

import { BaseRouter } from "./Base.router";
import UserController from "../controllers/UserController";
import checkAuth from "../middlewares/checkAuth";

class UserRouter extends BaseRouter {
  constructor() {
    super();
  }

  routes(): void {
    this.router.get("/", checkAuth, UserController.getDetails);
    this.router.post("/", checkAuth, UserController.addDetails);
    this.router.post("/uploadImage", checkAuth, UserController.uploadImage);
    this.router.get("/:handle", UserController.getUserDetails);
    this.router.post(
      "/notifications",
      checkAuth,
      UserController.markNotificationsRead
    );
  }

  exposeRoutes(): Router {
    this.routes();
    return this.router;
  }
}

export const userRouter = new UserRouter().exposeRoutes();
