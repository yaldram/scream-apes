import { Router } from "express";

import { BaseRouter } from "./Base.router";
import ScreamController from "../controllers/ScreamController";
import { screamRules } from "../rules/screams";
import checkAuth from "../middlewares/checkAuth";

class ScreamRouter extends BaseRouter {
  constructor() {
    super();
  }

  routes(): void {
    this.router.get("/screams", ScreamController.getScreams);
    this.router.get("/screams/:screamId", ScreamController.getScream);
    this.router.post(
      "/screams",
      checkAuth,
      screamRules["insertScream"],
      ScreamController.insertScream
    );
    this.router.post(
      "/screams/:screamId/comment",
      checkAuth,
      screamRules["insertScream"],
      ScreamController.insertComment
    );
    this.router.get(
      "/screams/:screamId/like",
      checkAuth,
      ScreamController.like
    );
    this.router.get(
      "/screams/:screamId/unlike",
      checkAuth,
      ScreamController.unlike
    );
    this.router.delete(
      "/screams/:screamId",
      checkAuth,
      ScreamController.deleteScream
    );
  }

  exposeRoutes(): Router {
    this.routes();
    return this.router;
  }
}

export const screamRouter = new ScreamRouter().exposeRoutes();
