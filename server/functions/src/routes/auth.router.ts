import { Router } from "express";

import { BaseRouter } from "./Base.router";
import AuthController from "../controllers/AuthController";
import { userRules } from "../rules/users";

class AuthRouter extends BaseRouter {
  constructor() {
    super();
  }

  routes(): void {
    this.router.post("/signup", userRules["register"], AuthController.signup);
    this.router.post("/login", userRules["login"], AuthController.login);
  }

  exposeRoutes(): Router {
    this.routes();
    return this.router;
  }
}

export const authRouter = new AuthRouter().exposeRoutes();
