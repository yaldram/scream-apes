import { Router } from "express";

interface IRouter {
  router: Router;
  routes(): void;
  exposeRoutes(): Router;
}

export abstract class BaseRouter implements IRouter {
  router: Router;

  constructor() {
    this.router = Router();
  }

  abstract routes(): void;
  abstract exposeRoutes(): Router;
}
