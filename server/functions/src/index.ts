import * as functions from "firebase-functions";

import app from "./app";
import { db } from "./init";

exports.api = functions.region("europe-west1").https.onRequest(app);

exports.createNotificationOnLike = functions
  .region("europe-west1")
  .firestore.document(`likes/{id}`)
  .onCreate(async snapshot => {
    try {
      const doc = await db.doc(`screams/${snapshot.data()!.screamId}`).get();
      if (
        doc.exists &&
        doc.data()!.userHandle !== snapshot.data()!.userHandle
      ) {
        await db.doc(`/notifications/${snapshot.id}`).set({
          createdAt: new Date().toISOString(),
          recipient: doc.data()!.userHandle,
          sender: snapshot.data()!.userHandle,
          type: "like",
          read: false,
          screamId: doc.id
        });
        return true;
      }
      return;
    } catch (error) {
      console.warn("Error on Like Notification", error);
      return false;
    }
  });

exports.deleteNotificationOnUnLike = functions
  .region("europe-west1")
  .firestore.document(`likes/{id}`)
  .onDelete(async sanpshot => {
    try {
      await db.doc(`notifications/${sanpshot.id}`).delete();
      return true;
    } catch (error) {
      console.warn("Error on Delete Like Notification", error);
      return false;
    }
  });

exports.createNotificationOnComment = functions
  .region("europe-west1")
  .firestore.document(`comments/{id}`)
  .onCreate(async snapshot => {
    try {
      const doc = await db.doc(`screams/${snapshot.data()!.screamId}`).get();
      if (
        doc.exists &&
        doc.data()!.userHandle !== snapshot.data()!.userHandle
      ) {
        await db.doc(`/notifications/${snapshot.id}`).set({
          createdAt: new Date().toISOString(),
          recipient: doc.data()!.userHandle,
          sender: snapshot.data()!.userHandle,
          type: "comment",
          read: false,
          screamId: doc.id
        });
        return true;
      }
      return;
    } catch (error) {
      console.warn("Error on Comment Notification", error);
      return false;
    }
  });

exports.onUserImageChange = functions
  .region("europe-west1")
  .firestore.document("users/{userId}")
  .onUpdate(async change => {
    try {
      if (change.before.data()!.imageUrl && change.after.data()!.imageUrl) {
        const batch = db.batch();
        const screamDoc = await db
          .collection("screams")
          .where("userHandle", "==", change.before.data()!.handle)
          .get();
        screamDoc.forEach(doc => {
          const scream = db.doc(`screams/${doc.id}`);
          batch.update(scream, { userImage: change.after.data()!.imageUrl });
        });
        await batch.commit();
        return true;
      }
      return;
    } catch (error) {
      console.warn("Error on User Image Change Trigger", error);
      return false;
    }
  });

exports.onScreamDelete = functions
  .region("europe-west1")
  .firestore.document("screams/{screamId}")
  .onDelete(async (snapshot, context) => {
    try {
      const screamId = context.params.screamId;
      const batch = db.batch();
      const comments = await db
        .collection("comments")
        .where("screamId", "==", screamId)
        .get();
      comments.forEach(comment =>
        batch.delete(db.doc(`comments/${comment.id}`))
      );
      const likes = await db
        .collection("likes")
        .where("screamId", "==", screamId)
        .get();
      likes.forEach(like => batch.delete(db.doc(`likes/${like.id}`)));
      const notifications = await db
        .collection("notifications")
        .where("screamId", "==", screamId)
        .get();
      notifications.forEach(notification =>
        batch.delete(db.doc(`notifications/${notification.id}`))
      );
      await batch.commit();
      return true;
    } catch (error) {
      console.warn("Error on User Image Change Trigger", error);
      return false;
    }
  });
