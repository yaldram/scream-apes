import * as admin from "firebase-admin";
import * as firebase from "firebase";

import { config } from "../../config";

export const firebaseAdmin = admin.initializeApp();

export const db = admin.firestore();

export const firebaseInit = firebase.initializeApp(config);
