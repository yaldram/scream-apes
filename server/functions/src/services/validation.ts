import { Request, Response } from "express";
import { validationResult } from "express-validator/check";

export default (req: any | Request, res: Response, errorCode: string) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({
      code: errorCode,
      errors: errors.array()
    });
    return true;
  }

  return false;
};
