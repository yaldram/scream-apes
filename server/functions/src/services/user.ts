function isEmpty(data: string) {
  if (data === "") {
    return true;
  }

  return false;
}

export const reduceUserDetails = (data: any) => {
  const userDetials: any = {};

  if (!isEmpty(data.bio.trim())) userDetials.bio = data.bio;

  if (!isEmpty(data.website.trim())) {
    if (data.website.trim().substring(0, 4) !== "http") {
      userDetials.website = `http://${data.website.trim()}`;
    } else {
      userDetials.website = data.website;
    }
  }

  if (!isEmpty(data.location.trim())) userDetials.location = data.location;
  return userDetials;
};
